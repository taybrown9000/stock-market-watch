import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class StockCSV {
	
	private CSVWriter writer;
	private CSVReader reader;
	private BufferedReader buffer;
	private boolean csvCleared = false;
	
	private URL yahooURL;
	private String csvFilename = "csv/stocks.csv";
	private String[] defaultStocks = {"GOOG", "MSFT", "AAPL", "PFE", "GE", "FB", "C", "F"};
	private List<String> currStockList;
	
	// constructor
	public StockCSV() {
		
		currStockList = new ArrayList<String>();
		createDefaultRows();
	}
	
	// sets up the table with 8 example stocks
	private void createDefaultRows() {
		
		for (String element : defaultStocks)
			addStock(element);
	}
	
	private boolean checkStockList(String symbol) {
		
		// iterate over the list to see if the symbol 
		// is already in the table or not
		for (String element : currStockList) {
			if (element.equals(symbol))
				return true;
		}
		
		return false;
	}

	// grabs a stock's symbol from the website
	// and writes it to our CSV file
	public boolean addStock(String symbol) {
		
		// if the stock symbol is not in the current stock list
		if (!checkStockList(symbol)) {

			// get its data online
			try {
				
				// if it's the first time launching the app, clear the csv file
				// otherwise, append to it
				if (!csvCleared) {
					writer = new CSVWriter(new FileWriter(csvFilename, false));
					csvCleared = true;
				} else
					writer = new CSVWriter(new FileWriter(csvFilename, true));
				
				// URL that we want to visit to grab the requested stock's data and
				// each parameter we're interested in
				yahooURL = new URL("http://finance.yahoo.com/d/quotes.csv?s=" + symbol + "&f=snl1d1t1ohgpv");
				
				// create a buffer stream to hold the data while we're reading it in from the website
				// if the website can't be reached, load default.csv
				if ( (buffer = new BufferedReader(new InputStreamReader(yahooURL.openStream()))) == null )
					csvFilename = "csv/default.csv";
					
				// tell the CSVReader that we want to use the BufferedReader we just created
				reader = new CSVReader(buffer);
				
				// nextLine[] is an array of values from the line
				String[] nextLine;

				// save any strings read into the array while the stream is not empty
				while ((nextLine = reader.readNext()) != null) {
					// put the stream array data into the buffer
					writer.writeNext(nextLine);
				}
				
				writer.close();
				
			} catch (IOException e) {
				// catch any errors when reading/writing
				e.printStackTrace();
			}

			// add the stock to the table list
			currStockList.add(symbol);
			
			// the operation was successful
			return true;
		}
		
		// if the stock is already in the list
		// don't add it twice
		else
			return false;
		
	}
	
	public void updateStocks() {
		
		try {

			// clear the CSV file for new data and create our writer
			writer = new CSVWriter(new FileWriter(csvFilename, false));
			
			// go through the current symbol list and grab new data, write to csv
			for (String symbol : currStockList) {
				// URL that we want to visit to grab the requested stock's data and
				// each parameter we're interested in
				yahooURL = new URL("http://finance.yahoo.com/d/quotes.csv?s=" + symbol + "&f=snl1d1t1ohgpv");

				// create a buffer stream to hold the data while we're reading it in from the website
				// if the website can't be reached, load default.csv
				buffer = new BufferedReader(new InputStreamReader(yahooURL.openStream()));

				// tell the CSVReader that we want to use the BufferedReader we just created
				reader = new CSVReader(buffer);

				// nextLine[] is an array of values from the line
				String[] nextLine;

				// save any strings read into the array while the stream is not empty
				while ((nextLine = reader.readNext()) != null) {
					// put the stream array data into the buffer
					writer.writeNext(nextLine);
				}

			}
			
			writer.close();

		} catch (IOException e) {
			// catch any errors when reading/writing
			e.printStackTrace();
		}

	}
	
	// removes a stock's symbol from our CSV file
	public boolean removeStock(String symbol) {
		
		// if the requested stock exists in the list
		if (checkStockList(symbol)) {
			
			// erase the row from the CSV file
			try {
				// create our CSVReader to read the file
				reader = new CSVReader(new FileReader(csvFilename));

				// grab all of the data in the current csv
				List<String[]> csvEntries = reader.readAll();

				// find the stock we want to remove in the list
				// and erase the row
				for (String[] array : csvEntries) {
					
					if (array[0].equals(symbol)) {
						csvEntries.remove(array);
						break;
					}
				}
				
				// create writer
				writer = new CSVWriter(new FileWriter(csvFilename, false)); 
				
				// write everything in our new list to file
				writer.writeAll(csvEntries);
				writer.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// remove the stock from the list
			currStockList.remove(symbol);
			
			// operation was successful
			return true;
		}
		
		// stock is not in the list, cannot remove
		// operation failed
		else 
			return false;
	}
	
	public List<String> getCurrStockList() {
		return currStockList;
	}
	
}
