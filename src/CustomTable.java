import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;


@SuppressWarnings("serial")
public class CustomTable extends JTable{
	
	final Color cBuy = Color.green;
	final Color cSell = Color.orange;
	final Color cHold = Color.cyan;
	StockStrategies strat;
	
	public CustomTable(){
		super();
		strat = new Strategy1();
		setModel(new CSVTableModel());
	}
	
	public CustomTable(int strategy){
		super();
		setModel(new CSVTableModel());
		
		switch(strategy){
		case 1:
			strat = new Strategy1();
			break;
		case 2:
			strat = new Strategy2();
			break;
		case 3:
			strat = new Strategy3();
			break;
		case 4:
			strat = new Strategy4();
			break;
		default:
			strat = new Strategy1();
			System.out.println("ERROR DETERMINING STRATEGY TYPE: STRATEGY 1 SELECTED BY DEFAULT");
			break;
		}

	}
	
	
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		int bsh[];
		
        Component stamp = super.prepareRenderer(renderer, row, column);
        bsh = strat.calculate(this);

        switch(bsh[row]){
        case 1:
        	stamp.setBackground(cBuy);
        	break;
        case 2:
        	stamp.setBackground(cSell);
        	break;
        case 3:
        	stamp.setBackground(cHold);
        	break;
        default:
        	stamp.setBackground(Color.black);
        	break;
        }


        return stamp;
	}
	
	
	
	
}//End CustomTable
