import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.table.DefaultTableModel;
import au.com.bytecode.opencsv.CSVReader;


public class CSVTableModel extends DefaultTableModel {
	
	public final Color color[] = new Color[]{Color.CYAN, Color.ORANGE, Color.YELLOW};
	private CSVReader reader;
	private String[] columnNames = {"Symbol", "Name", "Last", "Date", "Time", 
			"Open", "Daily High", "Daily Low", "Close Price", "Volume"};
	
	public CSVTableModel() {
		// set the table column headers
		for (String element : columnNames)
			addColumn(element);
		
		try {
			// create our CSVReader and read in row data
			reader = new CSVReader(new FileReader("csv/stocks.csv"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// create our CSV rows
		initRows();
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
	
	private void initRows() {
		String[] nextLine;
		
		// reads through the CSV file and adds each row to the table
		try {
			while ((nextLine = reader.readNext()) != null)
				addRow(nextLine);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
	
}
