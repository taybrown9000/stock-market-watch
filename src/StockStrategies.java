
public interface StockStrategies {

	public int[] calculate(CustomTable jt);
	
}//End StockStrategies


class Strategy1 implements StockStrategies{
	
	public int[] calculate(CustomTable jt){
		double last, close;
		int r[] = new int[jt.getRowCount()];
		
		for(int i = 0; i < jt.getRowCount() ;i++){//Loop through rows
			last = Double.parseDouble((String)jt.getValueAt(i, 2));
			close = Double.parseDouble((String)jt.getValueAt(i,8));
			if(last-close >= .05){
				r[i] = 1;
			}else if(close-last >= .05){
				r[i] = 2;
			}else{
				r[i] = 3;
			}
		}
		return r;
	}//End calculate
	
}//End Strategy1

class Strategy2 implements StockStrategies{
	
	public int[] calculate(CustomTable jt){
		double last, close;
		int r[] = new int[jt.getRowCount()];
		
		for(int i = 0; i < jt.getRowCount() ;i++){//Loop through rows
			last = Double.parseDouble((String)jt.getValueAt(i, 2));
			close = Double.parseDouble((String)jt.getValueAt(i,8));
			if(last-close >= .05){
				r[i] = 2;
			}else if(close-last >= .05){
				r[i] = 1;
			}else{
				r[i] = 3;
			}
		}
		return r;
	}//End calculate
	
}//End Strategy2

class Strategy3 implements StockStrategies{
	
	public int[] calculate(CustomTable jt){
		String symbol;
		int volume;
		int r[] = new int[jt.getRowCount()];
		
		for(int i = 0; i < jt.getRowCount() ;i++){//Loop through rows
			symbol = (String)jt.getValueAt(i, 0);
			volume = Integer.parseInt((String)jt.getValueAt(i, 9));
			
			if(symbol.length() == 4){
				r[i] = 1;
			}else if(volume%3 == 0){
				r[i] = 2;
			}else{
				r[i] = 3;
			}
		}
		return r;
	}//End calculate
	
}//End Strategy3

class Strategy4 implements StockStrategies{
	
	public int[] calculate(CustomTable jt){
		double high, low;
		int r[] = new int[jt.getRowCount()];

		for(int i = 0; i < jt.getRowCount() ;i++){//Loop through rows
			high = Double.parseDouble((String)jt.getValueAt(i, 5));
			low = Double.parseDouble((String)jt.getValueAt(i, 6));

			if(high-low >= 1.5){
				r[i] = 1;
			}else if(((int)high*(int)low)%2 == 0){
				r[i] = 2;
			}else{
				r[i] = 3;
			}
		}
		return r;
	}//End calculate
	
}//End Strategy4