import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Dialog;
import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


@SuppressWarnings("serial")
public class AddDialog extends JDialog {
	//AddDialog members
	private JTextField txtField;
	private String txtInput;
	private int clickType; //1=add, 2=delete
	private StockCSV stockCSV;
	
	public boolean success;
	public boolean isEmpty = false;
	
	//Constructor
	public AddDialog(){
		stockCSV = new StockCSV();
		
		//Set up AddDialog
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Add/ Remove a Stock");
		setSize(350,150);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
		
		//Add txtInput box to this
		txtField = new JTextField();
		txtField.setBounds(166, 24, 131, 30);
		txtField.setColumns(10);
		getContentPane().add(txtField);
		
		//Add label to this
		JLabel lblEnterStockSymbol = new JLabel("Enter stock symbol: ");
		lblEnterStockSymbol.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEnterStockSymbol.setBounds(25, 23, 131, 20);
		getContentPane().add(lblEnterStockSymbol);
		
		//Add button to add stock to the list
		JButton btnAdd = new JButton("Add Stock");
		btnAdd.setBounds(25, 72, 125, 25);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get txtInput from txtBox
				txtInput = txtField.getText();
				// convert txtInput to UPPERCASE
				txtInput = txtInput.toUpperCase();
				
				if(txtInput != null) {
					
					//ADD STOCK TO LIST

					// if the stock is not already in the list,
					// then add it and update the info box
					if (stockCSV.addStock(txtInput)) {
						clickType = 1;
						success = true;
						setVisible(false);
					}
					
					else
						success = false;
				}
				else
					isEmpty = true;
			}
		});
		getContentPane().add(btnAdd);
		
		//Add button to remove stock from the list
		JButton btnRemove = new JButton("Remove Stock");
		btnRemove.setBounds(172, 72, 125, 25);
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get txtInput from txtBox
				txtInput = txtField.getText();
				// convert txtInput to UPPERCASE
				txtInput = txtInput.toUpperCase();
				
				if(txtInput != null) {

					//REMOVE STOCK FROM LIST

					// if the stock is not already in the list,
					// then add it and update the info box
					if (stockCSV.removeStock(txtInput)) {
						clickType = 2;
						success = true;
						setVisible(false);
					}
					else
						success = false;
				}
				else
					isEmpty = true;
			}
		});
		getContentPane().add(btnRemove);
	}//End constructor
	
	//gettxtInput function
	public String getTxtInput(){
		return txtInput;
	}
	
	//clearText
	public void clearText(){
		txtField.setText(null);
	}
	
	public int getClickType(){
		return clickType;
	}

	public void updateStocks() {
		stockCSV.updateStocks();
	}
	
}//End AddDialog
