import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import au.com.bytecode.opencsv.CSVReader;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.UIManager.*;
import java.awt.SystemColor;

//Window class
public class Window {
	//Window members
	private JFrame frmStockMarketWatch;
	private JScrollPane jspStockList, jspStockAdvice;
	private CustomTable tblStockList;
	private JRadioButton btnStrategy1, btnStrategy2, btnStrategy3, btnStrategy4;
	private ButtonGroup strategyGroup;
	private JLabel lblStockList, lblStrategySelect, lblBuy, lblSell, lblHold;
	private JTextArea jtaStockAdvice;
	private JButton btnGetAdvice, btnAddRemove, btnUpdateStocks;
	private AddDialog Adialog;
	private CSVTableModel model;
	private JPanel colorPanel;


	//Main function (launch the application)
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmStockMarketWatch.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Constructor
	public Window() {

		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
		
		//Create instance of AddDialog to add to/remove from list
		Adialog = new AddDialog();
		
		createFrame();//Create and initialize frame
		createTable();//Create and initialize table (data is null)
		createButtonGroup();//Create radio buttons and add to strategyGroup
		createAdviceArea();//Create jta and button to calculate + display stock advice
		createLabels();//Create and add labels to frame
		
	}//End constructor
	
	//Create Frame
	private void createFrame(){
		frmStockMarketWatch = new JFrame();
		frmStockMarketWatch.getContentPane().setBackground(SystemColor.inactiveCaption);
		frmStockMarketWatch.setTitle("Stock Market Watch");
		frmStockMarketWatch.setSize(900,420);
		frmStockMarketWatch.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStockMarketWatch.getContentPane().setLayout(null);
		frmStockMarketWatch.setResizable(false);
		frmStockMarketWatch.setLocationRelativeTo(null);
	}//End createFrame
	
	//Create table and add it to Frame
	private void createTable(){
		
		jspStockList = new JScrollPane();
		jspStockList.setBounds(10, 40, 874, 185);
		frmStockMarketWatch.getContentPane().add(jspStockList);
		tblStockList = new CustomTable();
		jspStockList.setViewportView(tblStockList);
		tblStockList.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
	}//End createTable

	//Create strategy radio buttons and add them to strategyGroup
	private void createButtonGroup(){
		strategyGroup = new ButtonGroup(); //So only one strategy can be selected at a time
		
		btnStrategy1 = new JRadioButton("Buy: last-close > 5% | Sell: close-last > 5% | Hold: otherwise", true);//Strategy 1 selected by default
		btnStrategy1.setToolTipText("");
		btnStrategy1.setBounds(30, 275, 359, 23);
		frmStockMarketWatch.getContentPane().add(btnStrategy1);
		strategyGroup.add(btnStrategy1);
		
		btnStrategy2 = new JRadioButton("Buy: close-last > 5% | Sell: last-close > 5% | Hold: otherwise");
		btnStrategy2.setBounds(30, 301, 359, 23);
		frmStockMarketWatch.getContentPane().add(btnStrategy2);
		strategyGroup.add(btnStrategy2);
		
		btnStrategy3 = new JRadioButton("Buy: symbol = 4 characters | Sell: volume % 3 = 0 | Hold: otherwise");
		btnStrategy3.setBounds(30, 327, 396, 23);
		frmStockMarketWatch.getContentPane().add(btnStrategy3);
		strategyGroup.add(btnStrategy3);
		
		btnStrategy4 = new JRadioButton("Buy: high-low >= 5 | Sell: high*low % 2 = 0 | Hold: otherwise");
		btnStrategy4.setBounds(30, 353, 359, 23);
		frmStockMarketWatch.getContentPane().add(btnStrategy4);
		strategyGroup.add(btnStrategy4);
		
	}//End createButtonGroup
	
	//Create jta to display stock advice and button to calculate/display advice
	private void createAdviceArea(){
		
		//Create jsp and jta to hold stock advice
		jspStockAdvice = new JScrollPane();
		jspStockAdvice.setBounds(459,275,414,101);
		frmStockMarketWatch.getContentPane().add(jspStockAdvice);
		jtaStockAdvice = new JTextArea();
		jspStockAdvice.setViewportView(jtaStockAdvice);
		
		//Create button to calculate + display advice
		btnGetAdvice = new JButton("Get Advice");
		btnGetAdvice.setBackground(UIManager.getColor("Button.background"));
		btnGetAdvice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(btnStrategy1.isSelected()){
					updateTable(1);
				}else if(btnStrategy2.isSelected()){
					updateTable(2);
				}else if(btnStrategy3.isSelected()){
					updateTable(3);
				}else if(btnStrategy4.isSelected()){
					updateTable(4);
				}else{
					jtaStockAdvice.setText(jtaStockAdvice.getText() + "Error: No strategy selected" + "\n");
				}
			}
		});
		btnGetAdvice.setBounds(250, 236, 139, 25);
		frmStockMarketWatch.getContentPane().add(btnGetAdvice);
		
		Adialog.addWindowListener(new WindowAdapter() {//Add window listener to look for close event
			@Override
			public void windowDeactivated(WindowEvent arg0) {
				
				if (Adialog.getClickType() == 1) {
					//Add to list
					if (Adialog.success) {
						
						updateTable();
						jtaStockAdvice.setText(jtaStockAdvice.getText() + Adialog.getTxtInput() + " added to list" + "\n");
					}
					
					else 
						jtaStockAdvice.setText(jtaStockAdvice.getText() + Adialog.getTxtInput() + " already found in list."
								+ " Try a different stock symbol\n");

				} else if (Adialog.getClickType() == 2) {
					//Remove from list
					if (Adialog.success) {
						
						updateTable();
						jtaStockAdvice.setText(jtaStockAdvice.getText() + Adialog.getTxtInput() + " removed from list" + "\n");
					}
					
					else 
						jtaStockAdvice.setText(jtaStockAdvice.getText() + Adialog.getTxtInput() + " not found in list, try again\n");

				} else 
					jtaStockAdvice.setText(jtaStockAdvice.getText() + "Nothing entered, try again" + "\n");
			}
		});
		
		//Create button to call AddDialog
		btnAddRemove = new JButton("Add/ Remove Stock");//Button to add stock to list
		btnAddRemove.setBackground(new Color(255, 0, 0));
		btnAddRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Adialog.clearText();
				Adialog.setVisible(true);
			}
		});
		btnAddRemove.setBounds(399, 236, 150, 25);
		frmStockMarketWatch.getContentPane().add(btnAddRemove);

		btnUpdateStocks = new JButton("Update Stocks");
		btnUpdateStocks.setBackground(SystemColor.textHighlight);
		btnUpdateStocks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Adialog.updateStocks();
				updateTable();
				jtaStockAdvice.setText(jtaStockAdvice.getText() + "Stock list updated\n");
			}
		});
		btnUpdateStocks.setBounds(563, 236, 150, 25);
		frmStockMarketWatch.getContentPane().add(btnUpdateStocks);
		
		
		
	}//End createAdviceArea
	
	//Create labels for GUI and add them to Frame
	private void createLabels(){
		lblStockList = new JLabel("Stock List");
		lblStockList.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblStockList.setBounds(393, 11, 92, 18);
		frmStockMarketWatch.getContentPane().add(lblStockList);
		
		lblStrategySelect = new JLabel("Select Stock Strategy");
		lblStrategySelect.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblStrategySelect.setBounds(30, 254, 145, 19);
		frmStockMarketWatch.getContentPane().add(lblStrategySelect);
		
		colorPanel = new JPanel();
		colorPanel.setBackground(Color.BLACK);
		colorPanel.setForeground(Color.BLACK);
		colorPanel.setBounds(723, 227, 161, 30);
		frmStockMarketWatch.getContentPane().add(colorPanel);
		
		lblBuy = new JLabel("Buy");
		colorPanel.add(lblBuy);
		lblBuy.setForeground(Color.GREEN);
		lblBuy.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblBuy.setBackground(Color.GREEN);
		
		lblSell = new JLabel("Sell");
		colorPanel.add(lblSell);
		lblSell.setForeground(Color.ORANGE);
		lblSell.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		lblHold = new JLabel("Hold");
		colorPanel.add(lblHold);
		lblHold.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblHold.setForeground(Color.CYAN);
	}//End createLabels

	//Update table data
	private void updateTable(int strategy){
		tblStockList = new CustomTable(strategy);
		jspStockList.setViewportView(tblStockList);
		
	}
	
	private void updateTable(){
		tblStockList = new CustomTable();
		jspStockList.setViewportView(tblStockList);
		
	}
}//End Window
